<?php include('header.php'); ?> 
	<section id="product" class="gradient-layer">
		<div class="container product-top-container">
			<div class="top-features">
				<a class="back" href="#"><i class="fa fa-chevron-left"></i>back to results</a>
				<p>You have selected the Vibra-Trim VT500</p>
				<div class="btn-container">
					<a href="schedule.html" class="fancybox btn btn-default">Free Consultation</a>
					<a href="#" class=" btn-arrow-blue">Buy Now for $2500</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
					 <div class="product-slider">
						 <div class="main-img">
							<!-- <img class="main-slide-img" src="images/main-product.png" data-large="images/large/main-product.png" alt="jQuery"/> -->
							<a class="fancybox-popup" href="#popup" style="position:relative;">
								<span></span>
								<img src="images/main-product.png" class="main-image" />
							</a>
							<div class="below-image">
								<img src="images/product-below-image.png" />
							</div>
						</div>
						<div class="tmb-caption bx-slider">
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="jQuery плагин увеличения изображения"/>
							</div>
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="My the best jquery image zoom plugin"/>
							</div>
							<div class="slide wp-caption alignleft">
								<!-- <span class="product-slider-video-button"></span> -->
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="jQuery плагин зуммирования изображения"/>
							</div>
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="SergeLand Image Zoomer v3.0"/>
							</div>
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="jQuery плагин увеличения изображения"/>
							</div>
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="My the best jquery image zoom plugin"/>
							</div>
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="jQuery плагин зуммирования изображения"/>
							</div>
							<div class="slide wp-caption alignleft">
								<img src="images/main-product.png" data-tmb-large="images/large/main-product.png" alt="SergeLand Image Zoomer v3.0"/>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="product-tabs">
						<ul class="nav nav-tabs" role="tablist">
						  	<li class="active" rel="des"><a class="btn" href="#Description" role="tab" data-toggle="tab">Description</a></li>
						  	<li rel="spec"><a class="btn" href="#Specifications" role="tab" data-toggle="tab">Specifications</a></li>
						  	<li rel="review"><a class="btn" href="#Review" role="tab" data-toggle="tab">Review</a></li>
						  	<li rel="ship"><a class="btn" href="#shippingTerms " role="tab" data-toggle="tab">Shipping terms</a></li>
						  	<li rel="return"><a class="btn" href="#Returns" role="tab" data-toggle="tab">Returns</a></li>
						</ul>
						<div class="tab-content">
						  	<div class="tab-pane fade in active" id="Description">
						  		<p>The all new 3HP Vibratrim VT500 dual motion vibration machine is the latest in world class WBV equipment, with many advancements over previous Vibratrim models. It is a sophisticated vibration machine with new rotary speed dials, 58% more platform space versus the older version, and dual vibration mode that is upto 200% more efficient then single vibration machines!</p><br>
						  		<p>The VT500 is constructed using high gauge steel and the display is made of a dynamic digital tube with microcomputer intelligent controls with easily and quickly adjustable rotary speed knobsand dual vibration mode that is upto 200% more efficient then single vibration machines! <a class="more" href="#">more...</a></p>
							</div>
							<div class="tab-pane fade" id="Specifications">
								<div class="row">
									<div class="col-md-12">
										<div class="specs">
											<!-- LOADER BAR -->
											<div class="key-spec specification-key">
												<h4>Key Specs </h4>
												<ul id="loadbar" class="spec-progressbar">
												    <li><div id="layerFill1" class="bar"></div></li>
												    <li><div id="layerFill2" class="bar"></div></li>
												    <li><div id="layerFill3" class="bar"></div></li>
												    <li><div id="layerFill4" class="bar"></div></li>
												    <li><div id="layerFill5" class="bar"></div></li>
												    <li><div id="layerFill6" class="bar"></div></li>
												    <li><div id="layerFill7" class="bar"></div></li>
												    <li><div id="layerFill8" class="bar"></div></li>
												    <li><div id="layerFill9"></div></li>
												    <li><div id="layerFill10"></div></li>
											    	<span class="key-spec-text">Fitness 8</span>
											    </ul>
											<!-- END LOADER BAR -->
											
											<!-- LOADER BAR -->
												<ul id="loadbar" class="spec-progressbar">
												    <li><div id="layerFill1" class="bar"></div></li>
												    <li><div id="layerFill2" class="bar"></div></li>
												    <li><div id="layerFill3" class="bar"></div></li>
												    <li><div id="layerFill4" class="bar"></div></li>
												    <li><div id="layerFill5" class="bar"></div></li>
												    <li><div id="layerFill6" class="bar"></div></li>
												    <li><div id="layerFill7" class="bar"></div></li>
												    <li><div id="layerFill8" class="bar"></div></li>
												    <li><div id="layerFill9" class="bar"></div></li>
												    <li><div id="layerFill10"></div></li>
											    	<span class="key-spec-text">Bone Density 9</span>
											    </ul>
											<!-- END LOADER BAR -->	

											<!-- LOADER BAR -->
												<ul id="loadbar" class="spec-progressbar">
												    <li><div id="layerFill1" class="bar"></div></li>
												    <li><div id="layerFill2" class="bar"></div></li>
												    <li><div id="layerFill3" class="bar"></div></li>
												    <li><div id="layerFill4" class="bar"></div></li>
												    <li><div id="layerFill5" class="bar"></div></li>
												    <li><div id="layerFill6" class="bar"></div></li>
												    <li><div id="layerFill7" class="bar"></div></li>
												    <li><div id="layerFill8" class="bar"></div></li>
												    <li><div id="layerFill9" class="bar"></div></li>
												    <li><div id="layerFill10" class="bar"></div></li>
											    	<span class="key-spec-text">Detoxification 10</span>
											    </ul>
												<!-- END LOADER BAR -->	

											<!-- LOADER BAR -->
												<ul id="loadbar" class="spec-progressbar">
												    <li><div id="layerFill1" class="bar"></div></li>
												    <li><div id="layerFill2" class="bar"></div></li>
												    <li><div id="layerFill3" class="bar"></div></li>
												    <li><div id="layerFill4" class="bar"></div></li>
												    <li><div id="layerFill5" class="bar"></div></li>
												    <li><div id="layerFill6" class="bar"></div></li>
												    <li><div id="layerFill7" class="bar"></div></li>
												    <li><div id="layerFill8" class="bar"></div></li>
												    <li><div id="layerFill9" class="bar"></div></li>
												    <li><div id="layerFill10" class="bar"></div></li>
											    	<span class="key-spec-text">Lymph Drainage 10</span>
											    </ul>
											<!-- END LOADER BAR -->	
										</div>
											
											<p>The all new 3HP Vibratrim VT500 dual motion vibration machine is the latest in world class WBV equipment, with many advancements over previous Vibratrim models. It is a sophisticated vibration machine with new rotary speed dials, 58% more platform space versus the older version, and dual vibration mode that is upto 200% more efficient then single vibration machines!</p>
									  		<p>The VT500 is constructed using high gauge steel and the display is made of a dynamic digital tube with microcomputer intelligent controls with easily and quickly adjustable rotary speed knobsand dual vibration mode that is upto 200% more efficient then single vibration machines! <a class="more" href="#">more...</a></p>

										</div>
									</div>
							  	</div>
							</div>
							<div class="tab-pane fade" id="Review">
								<div class="row">
									<div class="md-col-12">
										<div class="tab-inner-main">
										<div class="tab-inner-review">
										<div class="specs">
											<h4>Review</h4>
											<div class="starRate pull-right">
												<span class="star-rating">
												  <input type="radio" value="1" name="rating"><i></i>
												  <input type="radio" value="2" name="rating"><i></i>
												  <input type="radio" value="3" name="rating"><i></i>
												  <input type="radio" value="4" name="rating"><i></i>
												  <input type="radio" value="5" name="rating"><i></i>
												</span>
											</div>
											<div class="reviews">
												<img src="images/review.png">
												<p>The all new 3HP Vibratrim VT500 dual motion based tion machine is the latest in world class WBV equipment...</p>
											</div>
										</div>
										</div>
										<span class="review">
						  					<p>The all new 3HP Vibratrim VT500 dual motion vibration machine is the latest in world class WBV equipment, with many advancements over previous Vibratrim models. It is a sophisticated vibration machine with new rotary speed dials, 58% more platform space versus the older version, and dual vibration mode that is upto 200% more efficient then single vibration machines!</p>
										</span>			
										</div>			  			
						  				<span class="quote-review">
						  					<span class="review-border"><img src="images/border_hor.png"/></span>
											<h1>Consumer Review</h1>
							  				<p>The VT500 is constructed using high gauge steel and the display is made of a dynamic digital tube with microcomputer intelligent controls with easily and quickly adjustable rotary speed knobsand dual vibration mode that is upto 200% more efficient then single vibration machines!</p>
							  			</span>			
									</div>
								</div>
							</div>
							<div class="tab-pane fade" id="shippingTerms">
						  		<p>The all new 3HP Vibratrim VT500 dual motion vibration machine is the latest in world class WBV equipment, with many advancements over previous Vibratrim models. It is a sophisticated vibration machine with new rotary speed dials, 58% more platform space versus the older version, and dual vibration mode that is upto 200% more efficient then single vibration machines!</p><br>
						  		<p>The VT500 is constructed using high gauge steel and the display is made of a dynamic digital tube with microcomputer intelligent controls with easily and quickly adjustable rotary speed knobsand dual vibration mode that is upto 200% more efficient then single vibration machines! <a class="more" href="#">more...</a></p>
							</div>
							<div class="tab-pane fade" id="Returns">
						  		<p>The all new 3HP Vibratrim VT500 dual motion vibration machine is the latest in world class WBV equipment, with many advancements over previous Vibratrim models. It is a sophisticated vibration machine with new rotary speed dials, 58% more platform space versus the older version, and dual vibration mode that is upto 200% more efficient then single vibration machines!</p><br>
						  		<p>The VT500 is constructed using high gauge steel and the display is made of a dynamic digital tube with microcomputer intelligent controls with easily and quickly adjustable rotary speed knobsand dual vibration mode that is upto 200% more efficient then single vibration machines! <a class="more" href="#">more...</a></p>
							</div>
						</div>
					</div>
					<div class="col-md-4 specs-container">
						<div class="specs">
							<h4>Key Specs </h4>
							<div class="key-spec"><img src="images/key-spec1.png" /><p>Fitness 8</p></div>
							<div class="key-spec"><img src="images/key-spec2.png" /><p>Bone density 9</p></div>
							<div class="key-spec"><img src="images/key-spec3.png" /><p>Detoxification 10</p></div>
							<div class="key-spec"><img src="images/key-spec3.png" /><p>Lymph Drainage 10</p></div>
						</div>
						<a class="more" href="#">all specs</a>
					</div>
					<div class="col-md-4 specs-container">
						<div class="specs">
							<h4>Review</h4>
							<div class="starRate pull-right">
								<span class="star-rating">
								  <input type="radio" value="1" name="rating"><i></i>
								  <input type="radio" value="2" name="rating"><i></i>
								  <input type="radio" value="3" name="rating"><i></i>
								  <input type="radio" value="4" name="rating"><i></i>
								  <input type="radio" value="5" name="rating"><i></i>
								</span>
							</div>
							<div class="reviews">
								<img src="images/review.png">
								<p>The all new 3HP Vibratrim VT500 dual motion based tion machine is the latest in world class WBV equipment...</p>
								
							</div>
							<a href="#" class="more">read review</a>
						</div>
					</div>
					<div class="col-md-4 specs-container">
						<div class="specs">
							<h4>Concerns</h4>
							<div class="concern">
								<a href="#">How much is shipping</a>
								<a href="#">Frequently asked questions</a>
								<a href="#">How can I get a discount</a>
								<a href="#">Returns policy</a>
								<a href="#">How can I avoid a scam</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="mid-product-section">
			<div class="container">
				<div class="row">
					<div class="col-md-8 seperator-right">
						<div class="depth-analysis">
							<h5>Vibratim VT500 in depth analysis</h5>
							<div class="pull-right"><img src="images/product-logo1.png" /></div>
							<p>Explanation of the Vibratim VT500 in holistic vibration product introduction statement. The 400 model (has only 500 watts and only goes up to 14 hertz (new 500 models goes to 22 hertz, much faster). For those that don't need the most powerful one this is a very popular model. It has 10mm amplitude. The new model has 11mm amplitude and way more G force and lymphatic flushing power.</p>
							<div class="text-right"><a href="#">continue</a></div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="facts">
									<h5>Vibratim VT500 facts</h5>
									<div class="facts-links">
										<a class="vibratim" href="#vibratim-facts">Vibra-Trim Reviews</a>
										<a class="vibratim" href="#vibratim-facts">Vibra-Trim Reliability</a>
										<a class="vibratim" href="#vibratim-facts">Construction</a>
										<a class="vibratim" href="#vibratim-facts">Noise</a>
										<a class="vibratim" href="#vibratim-facts">Size</a>
										<a class="vibratim" href="#vibratim-facts">Plate Size</a>
										<a class="vibratim" href="#vibratim-facts">For circulation</a>
										<a class="vibratim" href="#vibratim-facts">Bone Density</a>
										<a class="vibratim" href="#vibratim-facts">Lymphatic</a>
										<a class="vibratim" href="#vibratim-facts">Solclassness</a>
										<a class="vibratim" href="#vibratim-facts">Vibra-Trim Frequency range</a>
										<a class="vibratim" href="#vibratim-facts">Floor Vibration</a>
										<a class="vibratim" href="#vibratim-facts">Portability</a>
										<a class="vibratim" href="#vibratim-facts">Dual vibration</a>
										<a class="vibratim" href="#vibratim-facts">Usage</a>
										<a class="vibratim" href="#vibratim-facts">How to get it cheaper</a>
										<a class="vibratim" href="#vibratim-facts">With or without warranty</a>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="video-reviews">
									<h5>Top Vibratim VT500 video reviews</h5>
									<div class="tab-col">
					  					<a href="#">
					  						<img src="images/tab-pic1.png" />
					  						<div>
					  							<h5>G Force - Constant VS Peak</h5>
					  							<p>Learn about how blach blaLearn about how blach blahh Learn about how blach blah </p>
											</div>
										</a>
									</div>
									<div class="tab-col">
										<a href="#">
					  						<img src="images/tab-pic2.png" />
					  						<div>
					  							<h5>Engineering and Components</h5>
					  							<p>Learn about how blach blaLearn about how blach blahh Learn about how blach blah</p>
											</div>
										</a>
					  				</div>
					  				<div class="tab-col">
					  					<a href="#">
					  						<img src="images/tab-pic3.png" />
					  						<div>
					  							<h5>Manufacturing Sources</h5>
					  							<p>Learn about how blach blaLearn about how blach blahh Learn about how blach blah</p>
											</div>
										</a>
					  				</div>
					  				<div class="text-right"><a href="#">check out more videos</a></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4 compare-border">
						<div class="compare-model">
							<h5>Compare Vibratim VT500 with any model</h5>
							<select class="select">
								<option selected="selected">Choose Model</option>
								<option></option>
								<option></option>
							</select>
							<a class="grey-arrow-btn button-link" href="#">Compare</a>
						</div>
						<div class="common-comparison">
							<h5>Most common comparisons</h5>
							<div class="comp-col">
								<div class="dark-col">
									<img src="images/compare1.png" />
									<p>Vibra-Tirm VT500 3HP</p>
								</div>
								<div class="versus"><img src="images/vs.png"></div>
								<div class="light-col">
									<img src="images/compare2.png" />
									<p>Vmax Pulser 1100W</p>
								</div>
							</div>
							<div class="comp-col">
								<div class="dark-col">
									<img src="images/compare1.png" />
									<p>Vibra-Tirm VT500 3HP</p>
								</div>
								<div class="versus"><img src="images/vs.png"></div>
								<div class="light-col">
									<img src="images/compare2.png" />
									<p>Vmax Pulser 1100W</p>
								</div>
							</div>
							<div class="comp-col">
								<div class="dark-col">
									<img src="images/compare1.png" />
									<p>Vibra-Tirm VT500 3HP</p>
								</div>
								<div class="versus"><img src="images/vs.png"></div>
								<div class="light-col">
									<img src="images/compare2.png" />
									<p>Vmax Pulser 1100W</p>
								</div>
							</div>
							<div class="text-right"><a href="#">view all comparisons</a></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div id="popup" style="display:none;">
		<div class="popup-image">
			<img src="images/product1.png" />
		</div>	
	</div>

	<div id="vibratim-facts" style="display:none;">
		<div class="vibratim-facts-title">vibratim-facts</div>
		<div class="vibratim-facts-divider" style="display:block; text-align:center;"><img src="images/border_hor.png"></div>
		<div class="vibratim-facts-content">have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day have a nice day </div>
	</div>

	<script>
		jQuery(function(){
			$(".main-slide-img").imagezoomsl({ 
				
				descarea: ".big-caption", 				
				zoomrange: [1.68, 10],
				zoomstart: 1.68,
				cursorshadeborder: "10px solid black",
				magnifiereffectanimate: "fadeIn",	
			});
		  

			$(".tmb-caption img").click(function(){

			    var that = this;
				$(".main-slide-img").fadeOut(600, function(){
				
					$(this).attr("src", 	   $(that).attr("src"))
						   .attr("data-large", $(that).attr("data-tmb-large"))
						   .fadeIn(1000);				
				});

			    return false;
			});  
			 $('.bx-slider').bxSlider({
			    slideWidth: 55,
			    minSlides: 6,
			    maxSlides: 6,
			    moveSlides: 1,
			    slideMargin: 10,
			   pager: false,
			  });
		});
	</script>
	
<?php include('footer.php'); ?> 