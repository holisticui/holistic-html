<!DOCTYPE html>
<html>
<head>
	<title>Holistic</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="css/select2.css">
	<link rel="stylesheet" type="text/css" href="css/everslider.css">
	<link rel="stylesheet" type="text/css" href="css/googlesearch.css">
	<link rel="stylesheet" type="text/css" href="css/thumbelina.css">
	<!-- <link rel="stylesheet" type="text/css" href="css/cloudzoom.css"> -->
	<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
 	<script src="//code.jquery.com/jquery-1.11.0.min.js"></script> 
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox.js"></script>
	<script type="text/javascript" src="js/select2.min.js"></script>
	<script type="text/javascript" src="js/jquery.everslider.min.js"></script>
	<script type="text/javascript" src="js/jquery.slickhover.js"></script>
	<script type="text/javascript" src="js/jquery.bxslider.js"></script>
	 <!-- <script type="text/javascript" src="js/cloudzoom.js"></script>
	 <script type="text/javascript" src="js/thumbelina.js"></script> -->
	<script type="text/javascript" src="js/zoomsl-3.0.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-50f634c21af71b12"></script>
	<script type="text/javascript">var switchTo5x=true;</script>
	<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
	<script type="text/javascript">stLight.options({publisher: "ur-1fe1dd2-d31d-e959-895d-490e78109f19", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
	<script type="text/javascript">
		function googleTranslateElementInit() {
		  new google.translate.TranslateElement({pageLanguage: 'en', includedLanguages: 'de,en,es,fr,iw,ja,la,nl,pl,ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
		}
	</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</head>
<body>
<header>
	<div class="top-header">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div id="google_translate_element" class="google_translate"></div> 
					<script>
					(function() {
						var cx = '008099527256576458347:wbs3tvb0pcg';
						var gcse = document.createElement('script'); gcse.type = 'text/javascript';
						gcse.async = true;
						gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
						'//www.google.com/cse/cse.js?cx=' + cx;
						var s = document.getElementsByTagName('script')[0];
						s.parentNode.insertBefore(gcse, s);
					})();
					</script>
					<p>Holistic-Vibration - Explore a holistic approach to whole body vibration</p>
				</div>
				<div class="col-md-6 social-links">
					<div class="search-box">
						<div class="search">
							<i class="fa fa-search"></i>
						</div>
						<form role="form" class="search-field">
						  	<div class="form-group">
						    	<input type="search" class="form-control" id="search" placeholder="Search">
						  	</div>
						</form>
					</div>
					<div class="rss">
						<a href="#" class="rss-feed">
							<i class="fa fa-rss"></i>
							<span>RSS</span>
						</a>
						<div class="rss-tip">
							<a class="btn btn-primary btn-sm" href="#">Facebook Feed</a>
							<a class="btn btn-primary btn-sm" href="#">Twitter Feed</a>
							<a class="btn btn-primary btn-sm" href="sitemap.php">Sitemap</a>
						</div>
					</div>
					<a href="#" class="contact-link">
						Contact
					</a>
					<div class="social-icon">
						<a href="#"><i class="fa fa-facebook-square"></i></a>
						<a href="#"><i class="fa fa-youtube-square"></i></a>
						<a href="#"><i class="fa fa-twitter-square"></i></a>
						<a href="#"><i class="fa fa-google-plus-square"></i></a>
						<a href="#"><i class="fa fa-linkedin-square"></i></a>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<div class="main-header">
		<nav class="navbar navbar-default" role="navigation">
			<div class="container">
				<p>10% off when you <a class="fancybox" href="schedule.html">schedule</a> or call for a free consultation 1-800-838-18125</p>
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="images/logo.png"></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li class="active"><a href="index.php">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Reviews</a></li>
						<li><a href="#">Get Educated</a></li>
						<li><a href="comparison.php">Comparisons</a></li>
						<li><a href="#">Buyer's Guide</a></li>
						<li><a href="#">Support</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</header>