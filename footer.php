<footer>
	<div class="upper-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="footer-content">
						<p>We guarantee the lowest price on any machine and will beat any best price by at least $100.</p><p>30 days no questions asked return policy. Payment plans are available on all machines.</p>
					</div>
				</div>
				<div class="col-md-6">
					<div class="client-logos">
						<a href="#"><img src="images/ssl-certified.png" /></a>
						<a href="#"><img src="images/verisign-secured.png" /></a>
						<a href="#"><img src="images/paypal.png" /></a>
						<a href="#"><img src="images/lowest.png" /></a>
						<a href="#"><img src="images/money-back.png" /></a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="main-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="follow-link">
						<img src="images/follow.png" />
						<p>It's getink Removal on your preparation list for spring eaning</p>
					</div>
				</div>
				<div class="col-md-8">
					<div class="social-links">
						<div class="addthis_native_toolbox"></div>
						<div class="addthis_sharing_toolbox"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="lower-footer">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<p>&copy;Holistic-Vibration 2014</p>
				</div>
				<div class="col-md-8">
					<ul class="footer-menus">
						<li><a href="#">Home</a></li>
						<li><a href="#">About</a></li>
						<li><a href="#">Reviews</a></li>
						<li><a href="#">Comparisons</a></li>
						<li><a href="#">Buyer&#39;s Guide</a></li>
						<li><a href="#">Support</a></li>
						<li><a href="#">Contact</a></li>
						<li><a href="#">Sitemap</a></li>
						<li><a href="#">Policy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
</body>
</html>