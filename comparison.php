<?php include('header.php'); ?>

<section id="comp-product">
	<div class="container">
		<div id="breadcrum">
			<a href="index.php" id="bread">Home</a> > Comparison
		</div>
		<div id="the-title">
			<span><h5>Compare Products</h5></span>
		</div>
		<div class="row" id="border-row">
			<div class="col-md-5" id="myCol2">
				<img src="images/product2.png" alt="">
				<h5 class="pro-title">VIVOVIBE 460</h5>
				<div class="starRate1">
					<span class="star-rating1">
						<input type="radio" name="rating" value="1"><i></i>
						<input type="radio" name="rating" value="2"><i></i>
						<input type="radio" name="rating" value="3"><i></i>
						<input type="radio" name="rating" value="4"><i></i>
						<input type="radio" name="rating" value="5"><i></i>
					</span>
				</div>
				<span id="video-demo">
					<a href="#"><i class="fa fa-youtube-play"></i> VIDEO DEMO</a>
				</span>
			</div>
			<div class="col-md-2" id="myCol3">
				<img src="images/compare-arrow.png" width="50" id="compare-ico" />
			</div>
			<div class="col-md-5" id="myCol4">
				<img src="images/product3.png" alt="">
				<h5 class="pro-title">HYPERVIBE Performance</h5>
				<div class="starRate2">
					<span class="star-rating2">
						<input type="radio" name="rating" value="1"><i></i>
						<input type="radio" name="rating" value="2"><i></i>
						<input type="radio" name="rating" value="3"><i></i>
						<input type="radio" name="rating" value="4"><i></i>
						<input type="radio" name="rating" value="5"><i></i>
					</span>
				</div>
				<span id="video-demo">
					<a href="#"><i class="fa fa-youtube-play"></i> VIDEO DEMO</a>
				</span>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<table class="table table-hover table-striped table-bordered table-responsive">
			<tr><th colspan="3" class="row-title">Specifications</th></tr>
			<tr>
				<th class="table-head">Price</th>
				<td class="table-data">$3990 with FREE shipping</td>
				<td class="table-data">$2599 with FREE shipping</td>
			</tr>
			<tr>
				<th class="table-head">Availability</th>
				<td class="table-data">
				In Stock (Delivered in 2-3 business days.)
				<div class="alignright">
					<input type="submit" name="submit" value="BUY NOW" class="submit readmore"/>
				</div>
				</td>
				<td class="table-data">
				In Stock
				<div class="alignright">
					<input type="submit" name="submit" value="BUY NOW" class="submit readmore"/>
				</div>
				</td>
			</tr>
			<tr>
				<th class="table-head">Warranty of Hardware</th>
				<td class="table-data">2 years</td>
				<td class="table-data">2 years</td>
			</tr>
			<tr>
				<th class="table-head">Special Offer</th>
				<td class="table-data">Buy a new Vivo 460 for $3990 and get 1 FREE MINI BioMat $650 value + FREE Dealership $80 value, 2 free EMF Protection Pendants $200 value, Plus 1 Free Ionic Foot Spa Solo Model $1995 value or 1 FREE Clear light Curve Sauna $1795 value</td>
				<td class="table-data">Free Instructional DVD, Free Book on Whole Body Vibration Free hour consultation on detox and WBV, Free Exercise Chart, Free EMF Protection Pendant $160 value, 4 Free Cell Phone Protection Stickers $120 value, Free bottle of Probiotics from Body Biotics $55 value, Free container of Green Superfoods $50 value, Free Cold Laser $695 Value, Free Alkaline Brine Free Salt $40 Value, Free Care Package of Organic Living Raw Foods $50 value, Free BioMat Pillow from Richway $350 value or Free New Therasage Infrared Sauna $650 value</td>
			</tr>
		</table>
	</div>
</section>

<section>
	<div class="container">
		<table class="table table-hover table-striped table-bordered table-responsive">
			<tr><th colspan="3" class="row-title">General Features</th></tr>
			<tr>
				<th class="table-head">Fitness</th>
				<td class="table-data">10</td>
				<td class="table-data">10</td>
			</tr>
			<tr>
				<th class="table-head">Bone Density</th>
				<td class="table-data">10</td>
				<td class="table-data">10</td>
			</tr>
			<tr>
				<th class="table-head">Detoxification</th>
				<td class="table-data">10</td>
				<td class="table-data">10</td>
			</tr>
			<tr>
				<th class="table-head">Lymph Drainage</th>
				<td class="table-data">10</td>
				<td class="table-data">10</td>
			</tr>
			<tr>
				<th class="table-head">Hardware Weight</th>
				<td class="table-data">180 pounds</td>
				<td class="table-data">110 pounds</td>
			</tr>
			<tr>
				<th class="table-head">Power Consumption</th>
				<td class="table-data">25 hertz</td>
				<td class="table-data">28 hertz</td>
			</tr>
			<tr>
				<th class="table-head">Other Features</th>
				<td class="table-data">A unique feature on the Vivo 460 is if you stand too close to one side on the 460 the shaft mechanism has an auto shut off. You have to stand equal distance apart and it will let you know if you aren't. If someone loses thier balance or just puts more weight on one side than the other, it will instantly slow down.</td>
				<td class="table-data">The Hypervibe has a totally different motor than the Nitrofit although they are made in the same plant. Hypervibe has a patent on the formula they build their frame with and the tilt table and everything. So far, not one has dared to copy the Hypervibe. There have been no companies so far that have been able to get the vibration this high in the 2,000 dollar price range.</td>
			</tr>
		</table>
	</div>
</section>

<?php include('footer.php'); ?>