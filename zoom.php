<?php include('header.php'); ?>
	<script type = "text/javascript">
        CloudZoom.quickStart();

            // Initialize the slider.
            $(function(){
                $('#slider1').Thumbelina({
                    $bwdBut:$('#slider1 .left'), 
                    $fwdBut:$('#slider1 .right')
                });
            });

            
        </script>
        
        <style>

            /* div that surrounds Cloud Zoom image and content slider. */
            #surround {
                width:50%;
                min-width: 256px;
                max-width: 480px;
            }
            
            /* Image expands to width of surround */
            img.cloudzoom {
                width:100%;
            }
            
            /* CSS for slider - will expand to width of surround */
            #slider1 {
                margin-left:20px;
                margin-right:20px;
                height:119px;
                border-top:1px solid #aaa;
                border-bottom:1px solid #aaa;
                position:relative;
            }
            
        </style>
        <script type="text/javascript">

             // The following piece of code can be ignored.
             $(function(){
               $(window).resize(function() {
                   $('#info').text("Page width: "+$(this).width());
               });
               $(window).trigger('resize');
           });
             
            // The following piece of code can be ignored.
            if (window.location.hostname.indexOf("starplugins.") != -1) {
                var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-254857-7']);
                _gaq.push(['_trackPageview']);
                (function() {
                    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
                })();
            }

        </script>
    </head>
    <body>
        <div id="wrapper">





            <div id="surround">

                <img class="cloudzoom" alt ="Cloud Zoom small image" id ="zoom1" src="http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image1.jpg"
                data-cloudzoom='zoomSizeMode:"image", autoInside: 550 '>
                <div id="slider1">
                    <div class="thumbelina-but horiz left">&#706;</div>
                    <ul>
                        <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image1.jpg" 
                           data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image1.jpg' "></li>

                        <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image2.jpg" 
                               data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image2.jpg' "></li>

                        <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image3.jpg" 
                                   data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image3.jpg' "></li>

                                   <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image4.jpg" 
                                       data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image4.jpg' "></li>

                                       <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image5.jpg" 
                                           data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image5.jpg' "></li>

                                           <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image6.jpg" 
                                               data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image6.jpg' "></li>
                                               <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image6.jpg" 
                                                   data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image6.jpg' "></li>
                                                   <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image3.jpg" 
                                                       data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image3.jpg' "></li>

                                                       <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image4.jpg" 
                                                           data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image4.jpg' "></li>

                                                           <li><img class='cloudzoom-gallery' src="http://www.starplugins.com/sites/starplugins/images/jetzoom/size81/image5.jpg" 
                                                               data-cloudzoom ="useZoom:'.cloudzoom', image:'http://www.starplugins.com/sites/starplugins/images/jetzoom/large/image5.jpg' "></li>



                                                           </ul>
                                                           <div class="thumbelina-but horiz right">&#707;</div>
                                                       </div>

                                                   </div>

                                               </div>
<?php include('footer.php'); ?>