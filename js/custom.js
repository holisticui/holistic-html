$(document).ready(function() {
	$('.nav.nav-tabs li').click(function(i)
	{
  	 	var relation = $(this).attr('rel'); 
  	 	//$('#loadbar').removeClass('ins');	    
  	 	$('.spec-progressbar').removeClass('ins');
  	 	if(relation == "spec"){
  	 		

			$('.spec-progressbar').removeClass('ins').delay(10).queue(function(next){
				$(this).addClass('ins');
		        next();
			    return false;
			});
  	 	}
	});
});
$(document).ready(function(){
	$(".fancybox-popup").fancybox({
		'autoSize' : false,
		height: 300,
		width: 600
	});
	$(".vibratim").fancybox({
		'autoSize' : false,
		height: 300,
		width: 600
	});

	$(".fancybox").fancybox({
		type: 'iframe',
		height: 400,
		width: 400
	});
});


$(function(){
	$('.select').select2(); 
});

$(document).ready(function(){  
	$('#productCarousel').everslider({  
	    mode: 'carousel',  
	    moveSlides: 1,
	    itemWidth : 160,
	    itemHeight: 250,
	    itemMargin: 30
	});  
});
$(function () {
	$('.nav-tabs, .tabs').tab();
});
$(function(){
	$('#search').focus(function(){
		$('.search').css('z-index', 1);
	}).blur(function(){
    	$('.search').css('z-index', 0);
  	})
});

$(window).bind('scroll', function() {
         if ($(window).scrollTop() > 50) {
             $('.navbar.navbar-default').addClass('fixed');
             $('.top-header').addClass('fixed');
         }
         else {
             $('.navbar.navbar-default').removeClass('fixed');
             $('.top-header').removeClass('fixed');
         }
    });