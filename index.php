<?php include('header.php'); ?> 
	<section id="mainSlider">
		<div class="container">
			<div class="col1">
				<strong>Sort reviews by:</strong>Editors Rating : 
				<div class="starRate">
					<span class="star-rating">
					  <input type="radio" name="rating" value="1"><i></i>
					  <input type="radio" name="rating" value="2"><i></i>
					  <input type="radio" name="rating" value="3"><i></i>
					  <input type="radio" name="rating" value="4"><i></i>
					  <input type="radio" name="rating" value="5"><i></i>
					</span>
				</div>
			</div>
			<div class="col2">
				<span>Medical condition</span>
				<select class="select">
					<option selected="selected">Lymphatic Drainage</option>
					<option></option>
					<option></option>
				</select>
			</div>
			<div class="row">
				<div class="product-slider">
					<div id="productCarousel" class="everslider">  
						<ul class="es-slides">  
							<li>
								<a href="#">
									<img src="images/product1.png" alt="" class="p-img">
									<div class="featured-overlay">
										<h5>Vibra-Trim VT500</h5>
										<div>
											<img src="images/product-logo1.png">
										</div>
										<ul>
											<li>Fitness 8</li>
											<li>Bone density 9</li>
											<li>Detoxification 10</li>
											<li>Lymph Drainage 10</li>
										</ul>
									</div>
								</a>
								<div class="info">
									<a href="#"><i class="fa fa-youtube-play"></i>VIDEO DEMO</a>
									<a href="#"><i class="fa fa-info-circle"></i>MORE INFO</a>
								</div>
							</li>  
							<li>
								<a href="#">
									<img src="images/product2.png" alt="" class="p-img">
									<div class="featured-overlay">
										<h5>VIVOVIBE 460</h5>
										<div><img src="images/product-logo2.png"></div>
										<ul>
											<li>Fitness 8</li>
											<li>Bone density 9</li>
											<li>Detoxification 10</li>
											<li>Lymph Drainage 10</li>
										</ul>
									</div>
								</a>
								<div class="info">
									<a href="#"><i class="fa fa-youtube-play"></i>VIDEO DEMO</a>
									<a href="#"><i class="fa fa-info-circle"></i>MORE INFO</a>
								</div>
							</li>  
							<li>
								<a href="#">
									<img src="images/product3.png" alt="" class="p-img">
									<div class="featured-overlay">
										<h5>HYPERVIBE Performance</h5>
										<div><img src="images/product-logo3.png"></div>
									</div>
								</a>	
								<div class="info">
									<a href="#"><i class="fa fa-youtube-play"></i>VIDEO DEMO</a>
									<a href="#"><i class="fa fa-info-circle"></i>MORE INFO</a>
								</div>
							</li>  
							<li>
								<a href="#">
									<img src="images/product4.png" alt="" class="p-img">
									<div class="featured-overlay">
										<h5>ForeverFit 2-N-1 HP</h5>
									</div>
								</a>
								<div class="info">
									<a href="#"><i class="fa fa-youtube-play"></i>VIDEO DEMO</a>
									<a href="#"><i class="fa fa-info-circle"></i>MORE INFO</a>
								</div>
							</li>   
						</ul>
					</div>
				</div>
				<div class="view-all-products"><a href="#">view all reviews</a></div>
			</div> 
		</div>
	</section>
	<section id="comparisons">
		<div class="container">
			<h5>Explore Comparisons</h5>
			<div class="row">
				<div class="col-md-4 text-center">
					<div class="compare-module">
						<p>Compare any two holistic vibration machine models:</p>
						<select class="select">
							<option selected="selected">Choose Model</option>
							<option></option>
							<option></option>
						</select>
						<h2>VS</h2>
						<select class="select">
							<option selected="selected">Choose Model</option>
							<option></option>
							<option></option>
						</select>
						<div class="generate-btn"><a href="#">Generate Comparison</a></div>
					</div>
				</div>
				<div class="col-md-8">
					<div class="row center-div">
						<div class="col-md-4 comparing-cols">
							<div class="compare-img">
								<a href="#">
									<p>Most compared Spiral</p>
									<div class="first-vs col-md-4">
										<img src="images/compare-pr-1.png" /><br>
										<div class="compare-feature">
											<p>Vibra-Tirm Vibra-Tirm</p>
										</div>
									</div>
									<div class="vs col-md-4">
										<img src="images/compare-arrow.png" /><br>
										<div>
											<div class="compare-feature"><br>
												<h5 style="margin-top:14px">VS</h5>
											</div>
										</div>
									</div>
									<div class="second-vs col-md-4">
										<img src="images/compare-pr-1.png" /><br>
										<div class="compare-feature">
											<p>Vibra-Tirm Vibra-Tirm</p>
										</div>
									</div>
								</a>	
								<div class="text-right"><a href="#">go</a></div>
							</div>
						</div>
						<div class="col-md-4 comparing-cols">
							<div class="compare-img">
								<a href="#">
									<p>Most compared Spiral</p>
									<div class="first-vs col-md-4">
										<img src="images/compare-pr-1.png" /><br>
										<div class="compare-feature">
											<p>Vibra-Tirm</p>
										</div>
									</div>
									<div class="vs col-md-4">
										<img src="images/compare-arrow.png" /><br>
										<div>
											<div class="compare-feature"><br><br>
												<h5>VS</h5>
											</div>
										</div>
									</div>
									<div class="second-vs col-md-4">
										<img src="images/compare-pr-1.png" />	<br>
										<div class="compare-feature">
											<p>Vibra-Tirm</p>
										</div>
									</div>
								</a>	
								<div class="text-right"><a href="#">go</a></div>
							</div>
						</div>
						<div class="col-md-4 comparing-cols">
							<div class="compare-img">
								<a href="#">
									<p>Most compared Linear</p>
									<img src="images/compare-pr-1.png" />
									<img src="images/compare-arrow.png" />
									<img src="images/compare-pr-2.png" />
									<div>
										<div class="compare-feature">
											<p>Vibra-Tirm</p>
											<p>VT500 3HP</p>
										</div>
										<div class="compare-feature">
											<h5>VS</h5>
										</div>
										<div class="compare-feature">
											<p>Vmax Pulser</p>
											<p>1100W</p>
										</div>
									</div>
								</a>
								<div class="text-right"><a href="#">go</a></div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="text-right"><a href="#">view all popular comparisons</a></div>
		</div>
	</section>
	<section id="guide">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<h5>Buyer&#39;s Guide</h5>
				</div>
				<div class="col-md-8">
					<div class="report">
						<p><img src="images/report.png" />2014 Consumer Industry Report - The Truth about the Industry's Dirt Little Secrets Exposed.</p>
					</div>
				</div>
			</div>
			<div class="guide-tabs">
				<ul class="nav nav-tabs" role="tablist">
				  	<li class="active"><a href="#thingsToConsiderBeforeYouBuy" role="tab" data-toggle="tab"><img src="images/cart.png" />THINGS TO CONSIDER BEFORE YOU BUY</a></li>
				  	<li><a href="#claimsToAvoidBeforeYouBuy" role="tab" data-toggle="tab"><img src="images/claim.png" />CLAIMS TO AVOID BEFORE YOU BUY</a></li>
				  	<li><a href="#scamWatchAndUpdates" role="tab" data-toggle="tab"><img src="images/scam.png" />SCAM WATCH <br>AND UPDATES</a></li>
				</ul>
				<div class="tab-content">
				  	<div class="tab-pane fade in active" id="thingsToConsiderBeforeYouBuy">
				  		<div class="row">
				  			<div class="col-md-4">
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
					  		<div class="col-md-4">
					  			<div class="tab-col">
					  				<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
				  			<div class="col-md-4">
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
					  	</div>
				  	</div>
				  	<div class="tab-pane fade" id="claimsToAvoidBeforeYouBuy">
				  		<div class="row">
				  			<div class="col-md-4">
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
					  		<div class="col-md-4">
					  			<div class="tab-col">
					  				<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
				  			<div class="col-md-4">
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
					  	</div>
				  	</div>
				  	<div class="tab-pane fade" id="scamWatchAndUpdates">
				  		<div class="row">
				  			<div class="col-md-4">
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
					  		<div class="col-md-4">
					  			<div class="tab-col">
					  				<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
				  			<div class="col-md-4">
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic1.png" />
				  						<div>
				  							<h5>G Force - Constant VS Peak</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
								</div>
								<div class="tab-col">
									<a href="#">
				  						<img src="images/tab-pic2.png" />
				  						<div>
				  							<h5>Engineering and Components</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  				<div class="tab-col">
				  					<a href="#">
				  						<img src="images/tab-pic3.png" />
				  						<div>
				  							<h5>Manufacturing Sources</h5>
				  							<p>Learn about how blach blaLearn about how blach blahh</p>
										</div>
									</a>
				  				</div>
				  			</div>
					  	</div>
				  	</div>
				</div>
			</div>
			<div class="text-right"><a href="#">view all buyer info</a></div>
		</div>
	</section>
<?php include('footer.php'); ?>